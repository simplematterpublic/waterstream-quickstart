#!/bin/sh

#set -e
SCRIPT_DIR=`realpath $(dirname "$0")`

. $SCRIPT_DIR/config.sh

$KAFKA_TOPICS_COMMAND --zookeeper $ZOOKEEPER --create --topic $SESSION_TOPIC --partitions $NEW_TOPICS_PARTITIONS --replication-factor $NEW_TOPICS_REPLICATION_FACTOR --config cleanup.policy=compact --config min.compaction.lag.ms=60000 --config delete.retention.ms=600000
$KAFKA_TOPICS_COMMAND --zookeeper $ZOOKEEPER --create --topic $RETAINED_MESSAGES_TOPIC --partitions $NEW_TOPICS_PARTITIONS --replication-factor $NEW_TOPICS_REPLICATION_FACTOR --config cleanup.policy=compact --config min.compaction.lag.ms=60000 --config delete.retention.ms=600000
$KAFKA_TOPICS_COMMAND --zookeeper $ZOOKEEPER --create --topic $CONNECTION_TOPIC --partitions $NEW_TOPICS_PARTITIONS --replication-factor $NEW_TOPICS_REPLICATION_FACTOR --config cleanup.policy=delete --config retention.ms=600000
$KAFKA_TOPICS_COMMAND --zookeeper $ZOOKEEPER --create --topic $MESSAGES_TOPIC --partitions $NEW_TOPICS_PARTITIONS --replication-factor $NEW_TOPICS_REPLICATION_FACTOR --config retention.ms=$DEFAULT_MESSAGES_RETENTION
$KAFKA_TOPICS_COMMAND --zookeeper $ZOOKEEPER --create --topic $DEMO_MESSAGES_TOPIC --partitions $NEW_TOPICS_PARTITIONS --replication-factor $NEW_TOPICS_REPLICATION_FACTOR --config retention.ms=$DEMO_MESSAGES_RETENTION

