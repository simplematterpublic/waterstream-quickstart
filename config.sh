#!/bin/sh
#Config for the application

export WATERSTREAM_VERSION=1.0.2

#Kafka config
#============
export KAFKA_BOOTSTRAP_SERVERS=PLAINTEXT://localhost:9092
#Empty to disable transactional messages - a bit less guarantees, but much faster. To enable transactions specify
# a unique across all Kafka connections value.
export KAFKA_TRANSACTIONAL_ID=
#Default topic for messages - anything not matched by KAFKA_MESSAGES_TOPICS_PATTERNS goes here.
export MESSAGES_TOPIC=mqtt_messages
#Additional topics for messages and respective MQTT topic patterns.
#Comma-separated: kafkaTopic1:pattern1,kafkaTopic2:pattern2. Patterns follow the MQTT subscription wildcards rules
export DEMO_MESSAGES_TOPIC=mqtt_demo_messages
export KAFKA_MESSAGES_TOPICS_PATTERNS="${DEMO_MESSAGES_TOPIC}:demo/#"
#Retained messages topic - for messages which should be delivered automatically on subscription. Should be compacted.
export RETAINED_MESSAGES_TOPIC=mqtt_retained_messages
#Session state persistence topic. Should be compacted
export SESSION_TOPIC=mqtt_sessions
#Connections topic - for detecting concurrent connections with same client ID.
export CONNECTION_TOPIC=mqtt_connections
export KAFKA_STREAMS_APPLICATION_NAME="waterstream-kafka"
export KAFKA_STREAMS_STATE_DIRECTORY="/tmp/kafka-streams"
#Should it clean the local state directory when MQTTd starts
export KAFKA_RESET_STREAMS_ON_START=false
#Should it clean the local state directory when MQTTd stops
export KAFKA_RESET_STREAMS_ON_EXIT=false
#Queue length for reading messages from Kafka
export CENTRALIZED_CONSUMER_LISTENER_QUEUE=32

#MQTT settings
#=============
export MQTT_PORT=1883
#Size of thread pool for blocking operations
export MQTT_BLOCKING_THREAD_POOL_SIZE=10
#Size of queue for receiving messages - between network event handling loop and actual processing of the messages
export MAX_QUEUED_INCOMMING_MESSAGES=1000
#Maximal number of in-flight messages per client - QoS 1 or QoS 2 messages which are in the middle of the communication sequence.
export MQTT_MAX_IN_FLIGHT_MESSAGES=10

#Monitoring
#==========
#Port to expose the metrics in Prometheus format
export MONITORING_PORT=1884
export MONITORING_METRICS_ENDPOINT="/metrics"
#Should the metrics output also include standard JVM metrics
export MONITORING_INCLUDE_JAVA_METRICS=false

#SSL
export SSL_ENABLED=false
#export SSL_KEY_PATH=
#export SSL_CERT_PATH=

#Authentication
#USERS_FILE_PATH=

#JMX settings for debug and profiling
export JMX_OPTIONS=
#JMX_PORT=5000
#RMI_PORT=5001
#export JMX_OPTIONS="-Dcom.sun.management.jmxremote=true -Dcom.sun.management.jmxremote.port=$JMX_PORT -Dcom.sun.management.jmxremote.rmi.port=$RMI_PORT -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false"

#Kotlin coroutines thread pool size. Optimal coroutines threads  number is 2*CPU cores number
export COROUTINES_THREADS=16

#Admin config - for topic creation only
#=======================================
#1 day
export DEFAULT_MESSAGES_RETENTION=86400000
#1 hour
export DEMO_MESSAGES_RETENTION=3600000
export ZOOKEEPER=localhost:2181

export NEW_TOPICS_PARTITIONS=5
export NEW_TOPICS_REPLICATION_FACTOR=1

#Non-dockerized
#export KAFKA_HOME=<some location>/kafka_<version>
#export KAFKA_TOPICS_COMMAND=$KAFKA_HOME/bin/kafka-topics.sh
#Dockerized
export KAFKA_TOPICS_COMMAND="docker run -ti --network=host confluentinc/cp-kafka:4.0.1 /usr/bin/kafka-topics"

